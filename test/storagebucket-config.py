def generate_config(context):
    storage_name = context.properties['STORAGE_NAME']
    storage_region = context.properties['STORAGE_REGION']
    outputs = []
    resources = []
    resources.append({
        'name': storage_name,
        'type': 'storage.v1.bucket',
        'properties': {
            'region': storage_region,
            'sourceBucket': storage_name
        }
    })
    outputs.append({
        'name': f"{storage_name}-bucket-name",
        'value': f"$(ref.{storage_name}.name)"
    })
    return {'resources': resources, 'outputs': outputs}
