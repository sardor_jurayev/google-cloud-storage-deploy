FROM google/cloud-sdk:342.0.0-alpine

COPY pipe /usr/bin/

ENTRYPOINT ["/usr/bin/pipe.sh"]
